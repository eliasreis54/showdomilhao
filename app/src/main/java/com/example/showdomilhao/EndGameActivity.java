package com.example.showdomilhao;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.showdomilhao.models.Ranking;
import com.example.showdomilhao.models.User;
import com.example.showdomilhao.repository.RankingRepository;
import com.example.showdomilhao.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class EndGameActivity extends AppCompatActivity {

    ListView ranking;
    List<String> result = new ArrayList<>();

    void populateResult() {
        List<Ranking> rankings = RankingRepository.getAll();
        for (Ranking ranking : rankings) {
            User user = UserRepository.findUserById(ranking.getUser());
            String text = user.getName() + ": " + ranking.getPoints() + " pontos.";
            result.add(text);
        }
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_game);

        populateResult();

        ranking = findViewById(R.id.ranking);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, result);
        ranking.setAdapter(adapter);
    }
}