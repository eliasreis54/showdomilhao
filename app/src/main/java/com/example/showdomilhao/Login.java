package com.example.showdomilhao;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.showdomilhao.models.User;
import com.example.showdomilhao.repository.UserRepository;

public class Login extends AppCompatActivity {

    EditText email;
    EditText password;
    Button login;

    void navigateToHome(User u) {
        Intent intent = new Intent(Login.this, HomeActivity.class);
        intent.putExtra("user", u);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void login() {
        String email = this.email.getText().toString();
        String passsword = this.password.getText().toString();

        if (email == "" || passsword == "") {
            Toast.makeText(Login.this, "Por favor preencha todos os dados", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        User u = UserRepository.loginByEmailPassword(email, passsword);
        if (u == null) {
            Toast.makeText(Login.this, "Usuário não encontrado", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        navigateToHome(u);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.editTextTextEmailAddress);
        password = findViewById(R.id.editTextNumberPassword);
        login = findViewById(R.id.login);

        login.setOnClickListener(v -> login());

    }
}