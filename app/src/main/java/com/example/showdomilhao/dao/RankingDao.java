package com.example.showdomilhao.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.showdomilhao.models.Ranking;

import java.util.List;

@Dao
public interface RankingDao {
    @Query("SELECT * FROM Ranking ORDER BY points")
    List<Ranking> getRankings();

    @Query("SELECT * FROM Ranking WHERE user = :userId")
    Ranking getRankingBeUser(int userId);

    @Insert
    void insert(Ranking ranking);

    @Update
    void update(Ranking ranking);
}
