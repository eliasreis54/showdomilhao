package com.example.showdomilhao.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.showdomilhao.models.Question;

import java.util.List;

@Dao
public interface QuestionDao {
    @Query("SELECT * FROM question")
    List<Question> getAll();

    @Insert
    void insertAllQuestions(List<Question> question);
}
