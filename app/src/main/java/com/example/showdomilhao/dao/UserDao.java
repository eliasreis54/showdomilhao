package com.example.showdomilhao.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.showdomilhao.models.User;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user WHERE email = :email and password = :password")
    User findUserByEmailPassword(String email, String password);

    @Query("SELECT * FROM User WHERE uuid = :userId")
    User findUserById(int userId);

    @Insert
    void insertUser(User user);
}
