package com.example.showdomilhao.repository;

import com.example.showdomilhao.database.DatabaseInstance;
import com.example.showdomilhao.models.Ranking;
import com.example.showdomilhao.models.User;

import java.util.ArrayList;
import java.util.List;

public class RankingRepository {

    public static List<Ranking> getAll() {
        try {
            return DatabaseInstance.getInstance().rankingDao().getRankings();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static void addRanking(Ranking ranking) {
        try {
            DatabaseInstance.getInstance().rankingDao().insert(ranking);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateRankins(User user) {
        try {
            Ranking currentRanking = DatabaseInstance.getInstance().rankingDao()
                    .getRankingBeUser(user.getUuid());
            if (currentRanking == null) {
                Ranking r = new Ranking(user.getUuid(), 1);
                addRanking(r);
            } else {
                currentRanking.setPoints(currentRanking.getPoints()+1);
                DatabaseInstance.getInstance().rankingDao().update(currentRanking);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
