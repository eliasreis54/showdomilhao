package com.example.showdomilhao.repository;

import com.example.showdomilhao.database.DatabaseInstance;
import com.example.showdomilhao.models.Question;
import com.example.showdomilhao.models.User;

import java.util.ArrayList;
import java.util.List;

public class QuestionRespository {
    private static List<Question> questions = new ArrayList<>();

    private static void populate() {
        try {
            questions = DatabaseInstance.getInstance().questionDao().getAll();

            if (questions.size() == 0) {

                List<String> aq1 = new ArrayList<>();
                aq1.add("pomps");
                aq1.add("Elaine");
                aq1.add("Indios");
                Question q1 = new Question("Quem Descobriu o Brasil", aq1, "Indios");
                questions.add(q1);

                List<String> aq2 = new ArrayList<>();
                aq2.add("1");
                aq2.add("2");
                aq2.add("3");
                Question q2 = new Question("Quanto é 2 + 1", aq2, "3");
                questions.add(q2);
                DatabaseInstance.getInstance().questionDao().insertAllQuestions(questions);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int numberOfQuestions() {
        return questions.size() - 1;
    }

    public static Question getQuestion(int question) {
        if (questions.size() == 0) {
            populate();
        }
        return questions.get(question);
    }


}
