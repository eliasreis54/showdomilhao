package com.example.showdomilhao.repository;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.showdomilhao.database.DatabaseInstance;
import com.example.showdomilhao.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UserRepository {
    public static User findUserById(int id) {
        try {
            return DatabaseInstance.getInstance().userDao().findUserById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void addUser(User user) {
        try {
            DatabaseInstance.getInstance().userDao().insertUser(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static User loginByEmailPassword(String email, String password) {
        try {
            User user = DatabaseInstance.getInstance().userDao().findUserByEmailPassword(email, password);
            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
