package com.example.showdomilhao;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.showdomilhao.database.DatabaseInstance;
import com.example.showdomilhao.models.Question;
import com.example.showdomilhao.models.Ranking;
import com.example.showdomilhao.models.User;
import com.example.showdomilhao.repository.QuestionRespository;
import com.example.showdomilhao.repository.RankingRepository;

public class HomeActivity extends AppCompatActivity {

    TextView question;
    RadioGroup group;
    int currentQuestionIndex = 0;
    String correctAnswer = "";
    User currentUser;

    void navigateToEndGame() {
        Intent intent = new Intent( HomeActivity.this, EndGameActivity.class);
        intent.putExtra("user", currentUser);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    void updateQuestionText() {
        group = findViewById(R.id.rdGroup);

        if (currentQuestionIndex > 0 && currentQuestionIndex > QuestionRespository.numberOfQuestions()) {
            navigateToEndGame();
            currentQuestionIndex = 0;
        } else {
            Question q = QuestionRespository.getQuestion(currentQuestionIndex);
            question.setText(q.getQuestion());
            correctAnswer = q.getCorrect();

            group.removeAllViews();;
            int radioButtonId = 1;
            for (String s : q.getAnswers()) {
                RadioButton option = new RadioButton(HomeActivity.this);
                option.setId(radioButtonId);
                option.setText(s);
                group.addView(option);
                radioButtonId++;
            }

            group.setOnCheckedChangeListener((group, checkedId) -> {
                int checked = group.getCheckedRadioButtonId();
                RadioButton b = (RadioButton) group.getChildAt(checked - 1);
                String answer = b.getText().toString();
                if (answer.equals(correctAnswer)) {
                    Toast.makeText(HomeActivity.this, "Resposta correta", Toast.LENGTH_LONG).show();
                    currentQuestionIndex = currentQuestionIndex+1;
                    updateQuestionText();
                    updateRanking();
                } else {
                    Toast.makeText(HomeActivity.this, "Resposta errada", Toast.LENGTH_LONG).show();
                    navigateToEndGame();
                }
            });
        }
    }

    void updateRanking() {
        RankingRepository.updateRankins(currentUser);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        currentUser = (User) getIntent().getExtras().get("user");
        System.out.println(currentUser.getName());
        question = findViewById(R.id.question);


        updateQuestionText();
    }
}