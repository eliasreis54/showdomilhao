package com.example.showdomilhao;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ApplicationErrorReport;
import android.app.Person;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.showdomilhao.models.User;
import com.example.showdomilhao.repository.UserRepository;

public class CreateAccount extends AppCompatActivity {

    EditText email;
    EditText name;
    EditText password;
    Button createAccount;

    private void navigateToLogin() {
        Intent intent = new Intent(CreateAccount.this, Login.class);
        startActivity(intent);
    }

    private void create() {
        String email = this.email.getText().toString();
        String name = this.name.getText().toString();
        String password = this.password.getText().toString();

        if (email == "" || name == "" || password == "") {
            Toast.makeText(CreateAccount.this, "Por favor preencha todos os dados", Toast.LENGTH_LONG)
                    .show();
            return;
        }

        User user = new User(name, email, password);
        UserRepository.addUser(user);
        navigateToLogin();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        email = findViewById(R.id.editTextTextEmailAddress);
        name = findViewById(R.id.editTextTextPersonName);
        password = findViewById(R.id.editTextNumberPassword);
        createAccount = findViewById(R.id.createAccount);

        createAccount.setOnClickListener(v -> create());
    }
}