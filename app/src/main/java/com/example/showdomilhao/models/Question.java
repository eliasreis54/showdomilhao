package com.example.showdomilhao.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity
public class Question {
    @PrimaryKey(autoGenerate = true)
    private int uuid;

    @ColumnInfo(name = "qustion_name")
    private String question;

    @ColumnInfo(name = "answers")
    private List<String> answers;

    @ColumnInfo(name = "correct")
    private String correct;

    public Question(String question, List<String> answers, String correct) {
        this.question = question;
        this.answers = answers;
        this.correct = correct;
    }

    public int getUuid() {
        return uuid;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }
}
