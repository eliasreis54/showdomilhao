package com.example.showdomilhao.models;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

@Entity
public class Ranking {
    @PrimaryKey(autoGenerate = true)
    private int uuid;

    @ColumnInfo(name = "user")
    private int user;


    @ColumnInfo(name = "points")
    private int points;

    public Ranking(int user, int points) {
        this.user = user;
        this.points = points;
    }

    public void setUuid(int uuid) {
        this.uuid = uuid;
    }

    public int getUuid() {
        return uuid;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
