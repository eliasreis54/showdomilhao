package com.example.showdomilhao;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.showdomilhao.database.DatabaseInstance;

public class MainActivity extends AppCompatActivity {

    Button login;
    Button createAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseInstance.initDatabase(getApplicationContext());

        login = findViewById(R.id.login);
        createAccount = findViewById(R.id.createAccount);


        login.setOnClickListener(v -> {
            Intent intent1 = new Intent(MainActivity.this, Login.class);
            startActivity(intent1);
        });

        createAccount.setOnClickListener(v -> {
            Intent intent1 = new Intent(MainActivity.this, CreateAccount.class);
            startActivity(intent1);
        });
    }
}