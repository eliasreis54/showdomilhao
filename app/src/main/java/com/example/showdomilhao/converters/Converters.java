package com.example.showdomilhao.converters;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.TypeConverter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Converters {
    @TypeConverter
    public static List<String> fromString(String value) {
        List<String> data = new ArrayList<>();
        String[] items = value.split(",");

        for (String item : items) {
            data.add(item);
        }

        return data;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @TypeConverter
    public static String toString(List<String> list) {
        return String.join(",", list);
    }
}
