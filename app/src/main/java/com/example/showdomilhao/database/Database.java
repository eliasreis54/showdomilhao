package com.example.showdomilhao.database;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.example.showdomilhao.converters.Converters;
import com.example.showdomilhao.dao.QuestionDao;
import com.example.showdomilhao.dao.RankingDao;
import com.example.showdomilhao.dao.UserDao;
import com.example.showdomilhao.models.Question;
import com.example.showdomilhao.models.Ranking;
import com.example.showdomilhao.models.User;

@androidx.room.Database(entities = {User.class, Question.class, Ranking.class}, version = 1)
@TypeConverters(Converters.class)
public abstract class Database extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract QuestionDao questionDao();
    public abstract RankingDao rankingDao();
}
