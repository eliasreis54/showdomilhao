package com.example.showdomilhao.database;

import android.content.Context;

import androidx.room.Room;

public class DatabaseInstance {
    private static Database database = null;

    public static void initDatabase(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context, Database.class, "show-milhao")
                    .allowMainThreadQueries()
                    .build();
        }
    }

    public static Database getInstance() throws Exception {
        if (database == null) {
            throw new Exception("Database no initializes");
        }
        return database;
    }
}
